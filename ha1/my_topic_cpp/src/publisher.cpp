#include <ros/ros.h>
#include <lecture1_msgs/Time.h>
#include <std_msgs/Time.h>

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "publisher");
    ros::NodeHandle n;
    ros::Publisher p = n.advertise<lecture1_msgs::Time>("topic", 5);
    ros::Rate rate(0.1);
    int c = 0;
    while (ros::ok())
    {
        lecture1_msgs::Time msg;
        msg.stamp = ros::Time::now();
        msg.data = c++;
        p.publish(msg);
        rate.sleep();
    }
    return 0;
}


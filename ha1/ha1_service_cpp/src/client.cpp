#include <ros/ros.h>
#include <std_srvs/Empty.h>

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "client", ros::init_options::AnonymousName);
    ros::NodeHandle n;
    ros::service::waitForService("service", -1);
    ros::ServiceClient c = n.serviceClient<std_srvs::Empty>("service");
    std_srvs::Empty srv;
    ROS_INFO_STREAM("Calling empty service");
    if (c.call(srv))
    {
        ROS_INFO_STREAM("Empty response recieved");
    }
    else
    {
        ROS_ERROR("Service call failed");
    }
    return 0;		
}

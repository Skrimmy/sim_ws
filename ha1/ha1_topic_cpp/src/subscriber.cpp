#include <ros/ros.h>
#include <nav_msgs/Path.h>

void callback(const nav_msgs::Path::ConstPtr& msg)
{
    ROS_INFO_STREAM("SEQ: " << msg->header.seq << "frame_id: " << msg->header.frame_id);
    for (auto it = msg->poses.begin() ; it != msg->poses.end() ; ++it)
    {
    ROS_INFO_STREAM("Z: " << it->pose.position.z);
    }

}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "subscriber", ros::init_options::AnonymousName);
    ros::NodeHandle n;
    ros::Subscriber s = n.subscribe("topic", 5, callback);
    ros::spin();
    return 0;
}


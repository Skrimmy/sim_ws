#!/usr/bin/env python
import roslib; roslib.load_manifest('beginner_tutorials')
import rospy
import time
from std_msgs.msg import String
from nav_msgs.msg import Path
from lecture1_msgs.msg import Path

def callback(msg):
    rospy.loginfo("%i.%i : %d", msg.stamp.secs, msg.stamp.nsecs, msg.data)
    
def subscriber():
    rospy.init_node("subscriber", anonymous=True)
    rospy.Subscriber("topic", Path, callback)
    rospy.spin()

if __name__ == '__main__':
    subscriber()


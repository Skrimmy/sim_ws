#include <ros/ros.h>
#include <nav_msgs/Path.h>

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "publisher");
    ros::NodeHandle n;
    ros::Publisher p = n.advertise<nav_msgs::Path>("topic", 5);
    ros::Rate rate(0.1);
    int c = 0;
    while (ros::ok())
    {
        nav_msgs::Path msg;
        msg.header.seq = c++;
        msg.header.stamp = ros::Time::now();
	msg.header.frame_id = "we";
	geometry_msgs::PoseStamped pose0;
        pose0.header = msg.header;
	pose0.pose.position.x = 1;
	pose0.pose.position.y = 2;
	pose0.pose.position.z = 3;
	pose0.pose.orientation.x = 0.1;
	pose0.pose.orientation.y = 0.2;
	pose0.pose.orientation.z = 0.3;
	pose0.pose.orientation.w = 0.4;
        geometry_msgs::PoseStamped pose1;
        pose1.header = msg.header;
	pose1.pose.position.x = 6;
	pose1.pose.position.y = 5;
	pose1.pose.position.z = 4;
	pose1.pose.orientation.x = 0.6;
	pose1.pose.orientation.y = 0.5;
	pose1.pose.orientation.z = 0.4;
	pose1.pose.orientation.w = 0.9;
        msg.poses.push_back(pose0);
	msg.poses.push_back(pose1);
        p.publish(msg);
        rate.sleep();
    }
    return 0;
}


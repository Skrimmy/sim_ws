#!/usr/bin/env python
import rospy
import time
from std_msgs.msg import String
from nav_msgs.msg import Path


def publisher(data):
	global xAnt
        global yAnt
        global cont

        pose = PoseStamped()

        pose.header.frame_id = "main"
        pose.pose.position.x = float(data.pose.pose.position.x)
        pose.pose.position.y = float(data.pose.pose.position.y)
        pose.pose.position.z = float(data.pose.pose.position.z)
        pose.pose.orientation.x = float(data.pose.pose.orientation.x)
        pose.pose.orientation.y = float(data.pose.pose.orientation.y)
        pose.pose.orientation.z = float(data.pose.pose.orientation.z)
        pose.pose.orientation.w = float(data.pose.pose.orientation.w)

        if (xAnt != pose.pose.position.x and yAnt != pose.pose.position.y):
                pose.header.seq = path.header.seq + 1
                path.header.frame_id = "main"
                path.header.stamp = rospy.Time.now()
                pose.header.stamp = path.header.stamp
                path.poses.append(pose)
               

        cont = cont + 1

        rospy.loginfo("Hit: %i" % cont)
        if cont > max_append:
                path.poses.pop(0)

        pub.publish(path)

        xAnt = pose.pose.orientation.x
        yAnt = pose.pose.position.y
        return path

	           
	while not rospy.is_shutdown():
         p.publish(rospy.Path.now(), c)
         c += 1
         rate.sleep()

if __name__ == '__main__':
		global xAnt
         	global yAnt
         	global cont
         	xAnt = 0.0
         	yAnt = 0.0
      	 	cont = 0
    try:
        publisher()
    except rospy.ROSInterruptException:
        pass


